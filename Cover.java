import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * A cover page / menu.
 * 
 * @author Jack Kester
 * @version 1.0.2a
 */
public class Cover extends World
{

    /**
     * Constructor for objects of class Cover.
     * 
     */
    public Cover()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1200, 800, 1); 
        addObject(new Button(), 595, 484);
        addObject(new Button2(), 595, 552);
        addObject(new Button4(), 595, 620);
    }
}
