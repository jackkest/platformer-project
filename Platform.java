import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * A special block that the player can stand on but bounces off
 * when they hit the bottom edge.
 * 
 * @author Jack Kester   
 * @version 1.0.0a
 */
public class Platform extends Actor
{

    public void act() 
    {
    }    
}
