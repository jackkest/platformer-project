import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * A Character that can move around the world.
 * 
 * @author Jack Kester 
 * @version 1.0.9a
 */
public class Character extends Actor
{
    int vSpeed = 0; // Vertical speed
    int i = 0;
    int maxVel = 15;
    int xSpeed = 0; // Horizontal speed
    int accel = 1; // Acceleration factor
    boolean canMoveLeft = true;
    boolean canMoveRight = true;
    GreenfootImage Character = getImage();
    
    public void act() 
    {
    }
    
    public boolean isTouchingGround()
    {
        return getOneObjectAtOffset(0, (Character.getHeight() / 2), Block.class) != null;
    }
    
    public boolean isTouchingPlat()
    {
        return (getOneObjectAtOffset(0, (Character.getHeight() / 2) + 10, Platform.class) != null);
    }
    
    public boolean isTouchingPowerPlat()
    {
        return (getOneObjectAtOffset(0, (Character.getHeight() / 2) - 5, PowerPlat.class) != null);
    }
    
    public void fall()
    {
        if(!isTouchingGround())
        {
            if(vSpeed >= maxVel)
            {
                setLocation(getX(), getY() + vSpeed);
            }
            else
            {
                setLocation(getX(), getY() + vSpeed);
                vSpeed = vSpeed + accel;
            }
        }
        else if(!isTouchingPlat())
        {
            if(vSpeed >= maxVel)
            {
                setLocation(getX(), getY() + vSpeed);
            }
            else
            {
                setLocation(getX(), getY() + vSpeed);
                vSpeed = vSpeed + accel;
            }
        }
    }
    
    private void checkCollision()
    {
        if(getOneObjectAtOffset(0, -((Character.getHeight() / 2) + 1), Platform.class) != null)
        {
            vSpeed = 4;
            fall();
        }
        if(getOneObjectAtOffset((8), 0, Platform.class) != null)
        {
            canMoveRight = false;
        }
        else if(getOneObjectAtOffset((8), 0, Platform.class) == null)
        {
            canMoveRight = true;
        }
        if(getOneObjectAtOffset((-8), 0, Platform.class) != null)
        {
            canMoveLeft = false;
        }
        else if(getOneObjectAtOffset((-8), 0, Platform.class) == null)
        {
            canMoveLeft = true;
        }
        
    }
    
    public void unStuck()
    {
        if(isTouchingGround() || isTouchingPlat() || isTouchingPowerPlat()) 
        {
            Actor block = getOneObjectAtOffset(0, (Character.getHeight() / 2) + 20, Actor.class);
            if(block != null)
            {
                int ground = block.getImage().getHeight();
                int Y = (block.getY() - (ground + getImage().getHeight()) / 2) + 1;
                setLocation(getX(), Y);
            }
           
        }
    }    
}
