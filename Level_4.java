import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The forth level.
 * 
 * @author Cameron Honea
 * @version 1.0.1a
 */
public class Level_4 extends World
{

    /**
     * Constructor for objects of class Level_4.
     * 
     */
    public Level_4()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1200, 800, 1, false); 
        prepare();
    }
    
    public Level_4(Counter c)
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1200, 800, 1, false); 
        prepare();
        addObject(c, 1100, 12);
    }
    
    public int num()
    {
        return 4;
    }
    
    private void prepare()
    {
        Platform platform = new Platform();
        addObject(platform,38,764);
        Platform platform2 = new Platform();
        addObject(platform2,33,711);
        Platform platform3 = new Platform();
        addObject(platform3,39,656);
        Platform platform4 = new Platform();
        addObject(platform4,36,596);
        Platform platform5 = new Platform();
        addObject(platform5,34,564);
        Platform platform6 = new Platform();
        addObject(platform6,33,504);
        Platform platform7 = new Platform();
        addObject(platform7,32,450);
        Platform platform8 = new Platform();
        addObject(platform8,34,391);
        Platform platform9 = new Platform();
        addObject(platform9,40,331);
        Platform platform10 = new Platform();
        addObject(platform10,42,256);
        Platform platform11 = new Platform();
        addObject(platform11,41,184);
        Platform platform12 = new Platform();
        addObject(platform12,42,114);
        Platform platform13 = new Platform();
        addObject(platform13,41,50);
        platform.setLocation(34,771);
        platform.setLocation(33,771);
        platform3.setLocation(33,651);
        platform9.setLocation(37,304);
        platform8.setLocation(34,362);
        platform7.setLocation(33,428);
        platform6.setLocation(33,478);
        platform5.setLocation(35,517);
        platform4.setLocation(33,592);
        platform5.setLocation(33,533);
        platform6.setLocation(32,473);
        platform2.setLocation(33,713);
        platform3.setLocation(33,655);
        platform4.setLocation(33,596);
        platform4.setLocation(33,598);
        platform5.setLocation(33,540);
        platform6.setLocation(33,482);
        platform7.setLocation(33,425);
        platform8.setLocation(33,368);
        platform9.setLocation(33,311);
        platform10.setLocation(33,252);
        platform10.setLocation(33,254);
        platform11.setLocation(33,196);
        platform12.setLocation(33,138);
        platform13.setLocation(33,80);
        Platform platform14 = new Platform();
        addObject(platform14,101,779);
        Platform platform15 = new Platform();
        addObject(platform15,172,781);
        Platform platform16 = new Platform();
        addObject(platform16,180,721);
        Platform platform17 = new Platform();
        addObject(platform17,185,668);
        Platform platform18 = new Platform();
        addObject(platform18,186,611);
        Platform platform19 = new Platform();
        addObject(platform19,184,556);
        Platform platform20 = new Platform();
        addObject(platform20,182,505);
        Platform platform21 = new Platform();
        addObject(platform21,179,446);
        Platform platform22 = new Platform();
        addObject(platform22,179,396);
        Platform platform23 = new Platform();
        addObject(platform23,179,326);
        Platform platform24 = new Platform();
        addObject(platform24,175,278);
        Platform platform25 = new Platform();
        addObject(platform25,170,214);
        Platform platform26 = new Platform();
        addObject(platform26,168,156);
        Platform platform27 = new Platform();
        addObject(platform27,171,96);
        platform14.setLocation(91,772);
        platform15.setLocation(149,772);
        platform16.setLocation(149,714);
        platform17.setLocation(149,657);
        platform18.setLocation(149,599);
        platform19.setLocation(149,541);
        platform20.setLocation(149,483);
        platform21.setLocation(149,425);
        platform22.setLocation(149,367);
        platform23.setLocation(150,309);
        platform23.setLocation(149,309);
        platform24.setLocation(149,251);
        platform25.setLocation(149,193);
        platform26.setLocation(149,135);
        platform27.setLocation(149,77);
        Platform platform28 = new Platform();
        addObject(platform28,283,667);
        Platform platform29 = new Platform();
        addObject(platform29,280,584);
        Platform platform30 = new Platform();
        addObject(platform30,292,469);
        Platform platform31 = new Platform();
        addObject(platform31,295,315);
        Platform platform32 = new Platform();
        addObject(platform32,280,179);
        platform28.setLocation(113,598);
        platform29.setLocation(52,482);
        platform29.setLocation(66,483);
        platform30.setLocation(113,367);
        platform31.setLocation(295,315);
        platform31.setLocation(295,315);
        platform31.setLocation(295,315);
        platform31.setLocation(295,315);
        platform31.setLocation(295,315);
        platform31.setLocation(295,315);
        platform31.setLocation(295,315);
        platform31.setLocation(295,315);
        platform31.setLocation(295,315);
        platform31.setLocation(295,315);
        platform31.setLocation(295,315);
        platform31.setLocation(295,315);
        platform31.setLocation(295,315);
        platform31.setLocation(295,315);
        platform31.setLocation(295,315);
        platform31.setLocation(58,254);
        platform32.setLocation(120,134);
        Player player = new Player();
        addObject(player,98,682);
        Platform platform33 = new Platform();
        addObject(platform33,214,779);
        platform33.setLocation(207,772);
        Platform platform34 = new Platform();
        addObject(platform34,451,529);
        platform34.setLocation(444,523);
        Coin coin = new Coin();
        addObject(coin,153,27);
        Coin coin2 = new Coin();
        addObject(coin2,91,315);
        platform34.setLocation(384,526);
        platform34.setLocation(385,581);
        Platform platform35 = new Platform();
        addObject(platform35,256,765);
        Platform platform36 = new Platform();
        addObject(platform36,308,762);
        Platform platform37 = new Platform();
        addObject(platform37,360,764);
        platform37.setLocation(386,767);
        platform36.setLocation(324,771);
        platform35.setLocation(443,581);
        platform37.setLocation(443,638);
        platform36.setLocation(443,695);
        Platform platform38 = new Platform();
        addObject(platform38,431,752);
        platform38.setLocation(443,753);
        Platform platform39 = new Platform();
        addObject(platform39,450,778);
        platform39.setLocation(443,773);
        platform39.setLocation(443,774);
        Platform platform40 = new Platform();
        addObject(platform40,449,529);
        Platform platform41 = new Platform();
        addObject(platform41,451,469);
        Platform platform42 = new Platform();
        addObject(platform42,448,412);
        Platform platform43 = new Platform();
        addObject(platform43,451,356);
        Platform platform44 = new Platform();
        addObject(platform44,448,302);
        Platform platform45 = new Platform();
        addObject(platform45,448,244);
        Platform platform46 = new Platform();
        addObject(platform46,448,184);
        Platform platform47 = new Platform();
        addObject(platform47,448,129);
        platform40.setLocation(443,523);
        platform41.setLocation(443,466);
        platform42.setLocation(443,409);
        platform43.setLocation(443,351);
        platform44.setLocation(443,291);
        platform44.setLocation(443,291);
        platform44.setLocation(443,293);
        platform45.setLocation(443,236);
        platform46.setLocation(443,178);
        platform47.setLocation(443,120);
        platform47.setLocation(207,404);
        platform47.setLocation(268,405);
        Platform platform48 = new Platform();
        addObject(platform48,275,354);
        Platform platform49 = new Platform();
        addObject(platform49,270,300);
        Platform platform50 = new Platform();
        addObject(platform50,274,246);
        Platform platform51 = new Platform();
        addObject(platform51,271,197);
        platform48.setLocation(268,347);
        platform49.setLocation(268,289);
        platform50.setLocation(268,232);
        platform51.setLocation(268,174);
        Platform platform52 = new Platform();
        addObject(platform52,264,121);
        Platform platform53 = new Platform();
        addObject(platform53,264,74);
        Platform platform54 = new Platform();
        addObject(platform54,267,25);
        platform52.setLocation(268,118);
        platform53.setLocation(269,60);
        platform53.setLocation(268,61);
        platform54.setLocation(267,25);
        platform54.setLocation(267,25);
        platform54.setLocation(267,25);
        platform54.setLocation(267,25);
        platform54.setLocation(267,25);
        platform54.setLocation(267,25);
        platform54.setLocation(267,25);
        platform54.setLocation(275,0);
        platform54.setLocation(275,0);
        platform54.setLocation(275,0);
        platform54.setLocation(275,0);
        platform54.setLocation(275,0);
        platform54.setLocation(275,0);
        platform54.setLocation(268,3);
        Platform platform55 = new Platform();
        addObject(platform55,300,413);
        platform55.setLocation(283,405);
        PowerPlat powerplat = new PowerPlat();
        addObject(powerplat,450,80);
        powerplat.setLocation(428,56);
        powerplat.setLocation(412,54);
        powerplat.setLocation(394,53);
        Platform platform56 = new Platform();
        addObject(platform56,923,767);
        platform56.setLocation(916,759);
        Coin coin3 = new Coin();
        addObject(coin3,376,521);
        coin3.setLocation(385,521);
        Coin coin4 = new Coin();
        addObject(coin4,440,113);
        Platform platform57 = new Platform();
        addObject(platform57,1159,588);
        platform57.setLocation(1094,581);
        Platform platform58 = new Platform();
        addObject(platform58,1150,524);
        platform58.setLocation(1152,524);
        TpKey tpkey = new TpKey();
        addObject(tpkey,1160,465);
        Coin coin5 = new Coin();
        addObject(coin5,917,688);
        Coin coin6 = new Coin();
        addObject(coin6,1079,508);
        coin6.setLocation(1096,521);
        coin5.setLocation(916,696);
    }

}
