import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * A transparent actor that if clicked takes the user back to the cover page.
 * 
 * @author Jack Kester
 * @version 1.0.0a
 */
public class Button3 extends Actor
{
    /**
     * Act - do whatever the Button3 wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        if(Greenfoot.mouseClicked(this))
        {
            Greenfoot.playSound("select.wav");
            Greenfoot.setWorld(new Cover());
        }
    }    
}
