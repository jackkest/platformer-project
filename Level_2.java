import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The second level.
 * 
 * @author Hank West
 * @version 1.0.1a
 */
public class Level_2 extends World
{

    /**
     * Constructor for objects of class Level_2.
     * 
     */
    public Level_2(Counter c)
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1200, 800, 1, false); 
        prepare();
        
        
        addObject(c, 1100, 12);
    }
    
    public int num()
    {
        return 2;
    }
    
     public Level_2()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1200, 800, 1, false); 
        prepare();
    }
    
    private void prepare()
    {
        Platform platform = new Platform();
        addObject(platform,471,28);
        Platform platform2 = new Platform();
        addObject(platform2,478,93);
        Platform platform3 = new Platform();
        addObject(platform3,470,162);
        platform2.setLocation(470,86);
        platform3.setLocation(470,142);
        platform.setLocation(471,26);
        platform.setLocation(471,30);
        Platform platform4 = new Platform();
        addObject(platform4,417,142);
        Platform platform5 = new Platform();
        addObject(platform5,363,141);
        Platform platform6 = new Platform();
        addObject(platform6,526,143);
        Platform platform7 = new Platform();
        addObject(platform7,579,144);
        Platform platform8 = new Platform();
        addObject(platform8,23,312);
        Platform platform9 = new Platform();
        addObject(platform9,83,312);
        platform9.setLocation(79,313);
        Platform platform10 = new Platform();
        addObject(platform10,31,713);
        Platform platform11 = new Platform();
        addObject(platform11,89,713);
        Platform platform12 = new Platform();
        addObject(platform12,146,716);
        Platform platform13 = new Platform();
        addObject(platform13,206,722);
        platform13.setLocation(201,719);
        platform12.setLocation(146,714);
        platform13.setLocation(204,713);
        platform12.setLocation(146,713);
        Platform platform14 = new Platform();
        addObject(platform14,263,715);
        platform14.setLocation(262,712);
        Platform platform15 = new Platform();
        addObject(platform15,336,714);
        platform15.setLocation(315,712);
        platform14.setLocation(260,711);
        platform14.setLocation(260,713);
        platform15.setLocation(316,713);
        Platform platform16 = new Platform();
        addObject(platform16,909,716);
        Platform platform17 = new Platform();
        addObject(platform17,979,718);
        Platform platform18 = new Platform();
        addObject(platform18,1037,713);
        Platform platform19 = new Platform();
        addObject(platform19,1104,719);
        Platform platform20 = new Platform();
        addObject(platform20,1158,717);
        platform17.setLocation(965,716);
        platform18.setLocation(1021,716);
        platform19.setLocation(1077,716);
        platform20.setLocation(1130,716);
        Platform platform21 = new Platform();
        addObject(platform21,1184,711);
        platform21.setLocation(1182,716);
        Platform platform22 = new Platform();
        addObject(platform22,917,526);
        Platform platform23 = new Platform();
        addObject(platform23,973,528);
        platform23.setLocation(973,526);
        Platform platform24 = new Platform();
        addObject(platform24,1038,532);
        platform24.setLocation(1028,526);
        Platform platform25 = new Platform();
        addObject(platform25,692,357);
        Platform platform26 = new Platform();
        addObject(platform26,755,363);
        Platform platform27 = new Platform();
        addObject(platform27,804,355);
        platform26.setLocation(747,355);
        platform26.setLocation(745,358);
        platform27.setLocation(798,358);
        platform26.setLocation(744,357);
        platform27.setLocation(795,357);
        platform7.setLocation(577,143);
        platform.setLocation(470,28);
        platform5.setLocation(363,142);
        Platform platform28 = new Platform();
        addObject(platform28,623,233);
        Platform platform29 = new Platform();
        addObject(platform29,668,232);
        platform29.setLocation(668,233);
        Player player = new Player();
        addObject(player,392,23);
        PowerPlat powerplat = new PowerPlat();
        addObject(powerplat,159,509);
        TpKey tpkey = new TpKey();
        addObject(tpkey,572,62);
        tpkey.setLocation(553,85);
        Coin coin = new Coin();
        addObject(coin,490,583);
        Coin coin2 = new Coin();
        addObject(coin2,629,545);
        Coin coin3 = new Coin();
        addObject(coin3,750,610);
        Coin coin4 = new Coin();
        addObject(coin4,36,243);
        Coin coin5 = new Coin();
        addObject(coin5,1156,660);
        Coin coin6 = new Coin();
        addObject(coin6,951,472);
        Coin coin7 = new Coin();
        addObject(coin7,652,164);
        coin.setLocation(452,530);
        coin2.setLocation(611,491);
        coin3.setLocation(770,549);
        coin.setLocation(445,470);
        coin2.setLocation(620,408);
        coin.setLocation(445,407);
        coin6.setLocation(829,446);
        coin6.setLocation(848,446);
        player.setLocation(386,71);
        addObject(new Platform(), 830, 714);
        addObject(new Platform(), 780, 714);
        int i2 = 0;
        for(int i = 0; i < 100; i++)
        {
            addObject(new Platform(), i2, 0);
            i2 += getObjects(Platform.class).get(0).getImage().getWidth();
        }
    }
}
