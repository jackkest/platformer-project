import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * A page explaining the project.
 * 
 * @author Jack Kester
 * @version 1.0.1a
 */
public class AboutPage extends World
{

    /**
     * Constructor for objects of class AboutPage.
     * 
     */
    public AboutPage()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1200, 800, 1); 
        addObject(new Button3(), 72, 40);
    }
}
