import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * A transparent actor that if clicked starts the game.
 * 
 * @author Jack Kester
 * @version 1.0.0a
 */
public class Button extends Actor
{
    /**
     * Act - do whatever the Button wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        MouseInfo m = Greenfoot.getMouseInfo();
        
        if(m != null)
        {
            if((m.getX() >= 516 && m.getX() <= 680) && (m.getY() <= 500 && m.getY() >= 465))
            {
                setImage("circle.png");
            }
        
            if(!((m.getX() >= 516 && m.getX() <= 680) && (m.getY() <= 500 && m.getY() >= 465)))
            {
                setImage("transparent.png");
            }
        }
        
        
        if(Greenfoot.mouseClicked(this))
        {
            Greenfoot.playSound("select.wav");
            Greenfoot.setWorld(new Level_1());
        }
    }    
}
