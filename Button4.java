import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Button4 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Button4 extends Actor
{
    /**
     * Act - do whatever the Button4 wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        if(Greenfoot.mouseClicked(this))
        {
            Greenfoot.playSound("select.wav");
            Greenfoot.setWorld(new HighScores());
        }
        
        MouseInfo m = Greenfoot.getMouseInfo();
        
        if(m != null)
        {
            if((m.getX() >= 516 && m.getX() <= 680) && (m.getY() >= 600 && m.getY() <= 639))
            {
                setImage("circle2.png");
            }
        
            if(!((m.getX() >= 516 && m.getX() <= 680) && (m.getY() >= 600 && m.getY() <= 639)))
            {
                setImage("transparent.png");
            }
        }
    }    
}
