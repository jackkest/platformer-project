import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;

/**
 * Write a description of class MyWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MyWorld extends World
{

    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {    
        
        super(1200, 800, 1, false); 
        generateFloor();
        setBackground("ay.jpg");
        addObject(new Player(), 674, 245);
        addObject(new PowerPlat(), 600, 400);
        
        //for testing sounds, score etc.
        
        addObject(new Counter(), 1121, 12);
        for(int i = 0; i < 5; i++)
        {
            addObject(new Coin(), Greenfoot.getRandomNumber(1200), Greenfoot.getRandomNumber(800));
        }
        

        //prepare();
    }
    public void act()
    {
        if(getObjects(Player.class).isEmpty() != true)
        {
            if(getObjects(Player.class).get(0).level() == 2)
            {
                level_2();
            }
        }
    }

    public void generateFloor()
    {
        int i2 = 0;
        for(int i = 0; i < 100; i++)
        {
            addObject(new Block(), i2, 790);
            i2 += getObjects(Block.class).get(0).getImage().getWidth();
        }
    }

    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    /*
    private void prepare()
    {
        Platform Platform = new Platform();
        addObject(Platform,462,593);
        Platform Platform2 = new Platform();
        addObject(Platform2,515,593);
        Platform Platform3 = new Platform();
        addObject(Platform3,562,593);
        Platform Platform4 = new Platform();
        addObject(Platform4,615,593);
        Platform Platform5 = new Platform();
        addObject(Platform5,644,593);
        Platform Platform6 = new Platform();
        addObject(Platform6,695,593);
        Platform Platform7 = new Platform();
        addObject(Platform7,749,593);
        Platform Platform8 = new Platform();
        addObject(Platform8,806,592);
        Platform8.setLocation(814,598);
        Platform Platform9 = new Platform();
        addObject(Platform9,857,541);
        Platform Platform10 = new Platform();
        addObject(Platform10,910,488);
        Platform Platform11 = new Platform();
        addObject(Platform11,462,538);
        Platform Platform12 = new Platform();
        addObject(Platform12,962,435);
        Platform Platform13 = new Platform();
        addObject(Platform13,1016,382);
        Platform Platform14 = new Platform();
        addObject(Platform14,1066,329);
        Platform Platform15 = new Platform();
        addObject(Platform15,1122,329);
        Platform Platform16 = new Platform();
        addObject(Platform16,1179,329);
        Platform8.setLocation(787,589);
        Platform8.setLocation(798,602);
        Platform8.setLocation(798,604);
    }
    */
    
    private void level_2()
    {
        List objects = getObjects(Actor.class);
        if (objects != null)
        {
            removeObjects(objects);
        }
    }
}
