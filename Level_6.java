import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Level_6 here.
 * 
 * @author Hank West 
 * @version 1.0.1b
 */
public class Level_6 extends World
{

    /**
     * Constructor for objects of class Level_6.
     * 
     */
    public Level_6(Counter c)
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1200, 800, 1, false); 
        prepare();
        addObject(c, 1100, 12);
    }
    
    private void prepare()
    {
        Platform platform = new Platform();
        addObject(platform,91,51);
        Platform platform2 = new Platform();
        addObject(platform2,149,54);
        Platform platform3 = new Platform();
        addObject(platform3,208,52);
        Platform platform4 = new Platform();
        addObject(platform4,270,54);
        Platform platform5 = new Platform();
        addObject(platform5,92,112);
        Platform platform6 = new Platform();
        addObject(platform6,101,165);
        Platform platform7 = new Platform();
        addObject(platform7,96,223);
        Platform platform8 = new Platform();
        addObject(platform8,149,225);
        Platform platform9 = new Platform();
        addObject(platform9,209,225);
        Platform platform10 = new Platform();
        addObject(platform10,258,230);
        Platform platform11 = new Platform();
        addObject(platform11,276,168);
        Platform platform12 = new Platform();
        addObject(platform12,274,107);
        platform.setLocation(91,51);
        platform2.setLocation(149,51);
        platform3.setLocation(205,51);
        platform4.setLocation(260,51);
        platform5.setLocation(91,107);
        platform6.setLocation(91,165);
        platform7.setLocation(91,223);
        platform8.setLocation(149,224);
        platform9.setLocation(204,225);
        platform10.setLocation(258,225);
        platform11.setLocation(259,170);
        platform12.setLocation(260,108);
        platform11.setLocation(261,166);
        platform11.setLocation(260,166);
        platform10.setLocation(260,220);
        platform9.setLocation(204,221);
        platform8.setLocation(147,220);
        platform7.setLocation(90,218);
        platform7.setLocation(91,218);
        PowerPlat powerplat = new PowerPlat();
        addObject(powerplat,155,112);
        powerplat.setLocation(154,105);
        Player player = new Player();
        addObject(player,161,170);
        player.setLocation(154,163);
        Platform platform13 = new Platform();
        addObject(platform13,442,228);
        Platform platform14 = new Platform();
        addObject(platform14,450,292);
        Platform platform15 = new Platform();
        addObject(platform15,447,350);
        Platform platform16 = new Platform();
        addObject(platform16,504,224);
        Platform platform17 = new Platform();
       addObject(platform17,560,231);
        Platform platform18 = new Platform();
        addObject(platform18,616,231);
        Platform platform19 = new Platform();
        addObject(platform19,450,403);
        Platform platform20 = new Platform();
        addObject(platform20,509,409);
        Platform platform21 = new Platform();
        addObject(platform21,570,411);
        Platform platform22 = new Platform();
        addObject(platform22,631,415);
        Platform platform23 = new Platform();
        addObject(platform23,694,407);
        Platform platform24 = new Platform();
        addObject(platform24,685,346);
        Platform platform25 = new Platform();
        addObject(platform25,685,289);
        Platform platform26 = new Platform();
        addObject(platform26,685,239);
        platform14.setLocation(443,286);
        platform15.setLocation(443,344);
        platform19.setLocation(443,401);
        platform20.setLocation(499,401);
        platform21.setLocation(555,401);
        platform22.setLocation(611,401);
        platform23.setLocation(667,402);
        platform16.setLocation(500,229);
        platform17.setLocation(554,231);
        platform17.setLocation(557,229);
        platform18.setLocation(611,229);
        platform26.setLocation(667,229);
        platform25.setLocation(668,287);
        platform24.setLocation(669,345);
        platform24.setLocation(668,345);
        PowerPlat powerplat2 = new PowerPlat();
        addObject(powerplat2,560,256);
        Platform platform27 = new Platform();
        addObject(platform27,857,405);
        Platform platform28 = new Platform();
        addObject(platform28,918,410);
        Platform platform29 = new Platform();
        addObject(platform29,973,406);
        platform28.setLocation(915,405);
        platform29.setLocation(973,404);
        Platform platform30 = new Platform();
        addObject(platform30,1033,409);
        platform30.setLocation(1028,405);
        Platform platform31 = new Platform();
        addObject(platform31,857,465);
        platform31.setLocation(857,462);
        Platform platform32 = new Platform();
        addObject(platform32,856,520);
        platform32.setLocation(857,520);
        Platform platform33 = new Platform();
        addObject(platform33,858,577);
        Platform platform34 = new Platform();
        addObject(platform34,913,577);
        Platform platform35 = new Platform();
        addObject(platform35,1029,459);
        Platform platform36 = new Platform();
        addObject(platform36,1033,519);
        Platform platform37 = new Platform();
        addObject(platform37,1034,579);
        Platform platform38 = new Platform();
        addObject(platform38,974,580);
        platform38.setLocation(970,578);
        platform36.setLocation(1027,516);
        platform37.setLocation(1027,573);
        platform37.setLocation(1027,578);
        platform38.setLocation(970,577);
        platform36.setLocation(1027,520);
        platform35.setLocation(1027,464);
        platform30.setLocation(1028,406);
        platform29.setLocation(973,405);
        Coin coin = new Coin();
        addObject(coin,348,257);
        Coin coin2 = new Coin();
        addObject(coin2,767,458);
        PowerPlat powerplat3 = new PowerPlat();
        addObject(powerplat3,896,452);
        powerplat3.setLocation(919,435);
        Platform platform39 = new Platform();
        addObject(platform39,1160,707);
        Platform platform40 = new Platform();
        addObject(platform40,974,760);
        Platform platform41 = new Platform();
        addObject(platform41,797,759);
        Platform platform42 = new Platform();
        addObject(platform42,620,699);
        Platform platform43 = new Platform();
        addObject(platform43,445,640);
        Platform platform44 = new Platform();
        addObject(platform44,267,583);
       Platform platform45 = new Platform();
        addObject(platform45,88,699);
        Platform platform46 = new Platform();
        addObject(platform46,208,702);
        Platform platform47 = new Platform();
        addObject(platform47,266,700);
        Platform platform48 = new Platform();
        addObject(platform48,325,702);
        platform47.setLocation(267,701);
        platform46.setLocation(211,700);
        Coin coin3 = new Coin();
        addObject(coin3,1161,604);
        Coin coin4 = new Coin();
        addObject(coin4,973,659);
        Coin coin5 = new Coin();
        addObject(coin5,973,659);
        Coin coin6 = new Coin();
        addObject(coin6,630,594);
        coin5.setLocation(797,639);
        TpKey tpkey = new TpKey();
        addObject(tpkey,279,763);
        platform46.setLocation(212,759);
        Powerup powerup = new Powerup();
        addObject(powerup,96,299);
        Coin coin7 = new Coin();
        addObject(coin7,266,514);
        coin.setLocation(354,215);
        coin2.setLocation(767,442);
        coin.setLocation(356,165);
        platform40.setLocation(998,794);
        coin4.setLocation(993,656);
        powerplat3.setLocation(888,437);
        Platform platform49 = new Platform();
        addObject(platform49,1187,712);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,706);
        platform49.setLocation(1198,707);
        powerplat3.setLocation(915,429);
        platform41.setLocation(877,789);
        coin5.setLocation(862,665);
        Platform platform50 = new Platform();
        addObject(platform50,754,769);
        coin5.setLocation(825,655);
        Platform platform51 = new Platform();
        addObject(platform51,172,646);
        powerup.setLocation(92,622);
        Platform platform52 = new Platform();
        addObject(platform52,20,371);
        Platform platform53 = new Platform();
        addObject(platform53,83,362);
        Platform platform54 = new Platform();
        addObject(platform54,139,372);
        Platform platform55 = new Platform();
        addObject(platform55,194,372);
        Platform platform56 = new Platform();
        addObject(platform56,243,372);
        Platform platform57 = new Platform();
        addObject(platform57,301,372);
        Platform platform58 = new Platform();
        addObject(platform58,350,371);
        Platform platform59 = new Platform();
        addObject(platform59,393,371);
        platform59.setLocation(399,374);
        platform58.setLocation(350,373);
        platform58.setLocation(357,372);
        platform59.setLocation(415,372);
        platform53.setLocation(78,371);
        platform54.setLocation(136,372);
        coin2.setLocation(769,404);
        Platform platform60 = new Platform();
        addObject(platform60,447,176);
        Platform platform61 = new Platform();
        addObject(platform61,502,174);
        Platform platform62 = new Platform();
        addObject(platform62,564,174);
        Platform platform63 = new Platform();
        addObject(platform63,628,175);
        platform62.setLocation(557,174);
        platform63.setLocation(612,175);
        Platform platform64 = new Platform();
        addObject(platform64,667,174);
        platform60.setLocation(443,174);
        platform61.setLocation(499,174);
        Platform platform65 = new Platform();
        addObject(platform65,860,347);
        Platform platform66 = new Platform();
        addObject(platform66,919,347);
        Platform platform67 = new Platform();
        addObject(platform67,978,348);
        Platform platform68 = new Platform();
        addObject(platform68,1032,351);
        platform68.setLocation(1028,351);
        platform67.setLocation(973,348);
        platform66.setLocation(916,347);
        platform66.setLocation(915,351);
        platform65.setLocation(857,351);
        platform67.setLocation(970,350);
        platform18.setLocation(583,316);
        removeObject(platform18);
        removeObject(platform16);
        removeObject(platform17);
        removeObject(platform28);
        removeObject(platform29);
        powerplat3.setLocation(920,405);
        powerplat2.setLocation(508,230);
        coin2.setLocation(770,370);
        platform48.setLocation(323,701);
        Coin coin8 = new Coin();
        addObject(coin8,213,708);
        Coin coin9 = new Coin();
        addObject(coin9,174,591);
        Coin coin10 = new Coin();
        addObject(coin10,448,582);
        Coin coin11 = new Coin();
        addObject(coin11,568,235);
        Coin coin12 = new Coin();
        addObject(coin12,609,235);
        Coin coin13 = new Coin();
        addObject(coin13,610,302);
        Coin coin14 = new Coin();
        addObject(coin14,611,349);
        Coin coin15 = new Coin();
        addObject(coin15,570,352);
        Coin coin16 = new Coin();
        addObject(coin16,567,298);
        Coin coin17 = new Coin();
        addObject(coin17,510,295);
        Coin coin18 = new Coin();
        addObject(coin18,510,347);
        coin14.setLocation(612,348);
        coin13.setLocation(611,292);
        coin16.setLocation(564,290);
        coin15.setLocation(562,349);
        Coin coin19 = new Coin();
        addObject(coin19,982,412);
        Coin coin20 = new Coin();
        addObject(coin20,978,484);
        Coin coin21 = new Coin();
        addObject(coin21,974,528);
        Coin coin22 = new Coin();
        addObject(coin22,925,525);
        Coin coin23 = new Coin();
        addObject(coin23,928,472);
        coin20.setLocation(975,469);
        Coin coin24 = new Coin();
        addObject(coin24,317,164);
        Coin coin25 = new Coin();
        addObject(coin25,398,196);
    }
}
