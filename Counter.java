import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
import java.util.ArrayList;

/**
 * A counter that keeps score.
 * 
 * @author Jack Kester 
 * @version 1.0.5a
 */
public class Counter extends Actor
{
    private int score = 0;
    UserInfo myInfo = UserInfo.getMyInfo();
    int[] scores = new int[5];
    
    public Counter()
    {
        setImage(new GreenfootImage("Score: 0", 24, Color.BLUE, Color.LIGHT_GRAY));
    }
    
    public Counter(int s)
    {
        setImage(new GreenfootImage("Score: " + s, 24, Color.BLUE, Color.LIGHT_GRAY));
    }
    
    public void act() 
    {
        storeScore();
        setImage(new GreenfootImage("Score: " + score, 24, Color.BLUE, Color.LIGHT_GRAY));
    }    
    
    public void increaseScore()
    {
        score = score + 25;
    }
    
    public int score()
    {
        return score;
    }
    
    public int scores(int ind)
    {
        //scores[0] = score;
        if(score >= scores[0])
        {
            scores[0] = score;
        }
        return scores[ind];
    }

    public void storeScore()
    {
        /*
            if(UserInfo.isStorageAvailable())
            {
                if(score > myInfo.getScore())
                {
                    myInfo.setScore(score);
                }
            }
            */
           myInfo.setScore(score);
    }
}
