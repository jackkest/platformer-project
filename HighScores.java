import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * A world that displays highscores.
 * 
 * @author Jack Kester
 * @version 1.0.1a
 */
public class HighScores extends World
{

    /**
     * Constructor for objects of class HighScores.
     * 
     */
    public HighScores()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1200, 800, 1, false); 
        addObject(new Button3(), 72, 40);
        addObject(new Counter(), 600, 950);
        addObject(new Score(), 600, 400);
        Score score1 = (Score)getObjectsAt(600, 400, Score.class).get(0);
        score1.generateImage(0);
    }
}
