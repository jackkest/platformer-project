import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Level_3 here.
 * 
 * @author Sean O'Connor
 * @version 1.0.1a
 */
public class Level_3 extends World
{

    /**
     * Constructor for objects of class Level_3.
     * 
     */
    public Level_3()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1200, 800, 1, false);
        prepare();
    }
    
    public Level_3(Counter c)
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1200, 800, 1, false); 
        prepare();
        addObject(c, 1100, 12);
    }
    
    public int num()
    {
        return 3;
    }
    
     private void prepare()
    {
        Block block = new Block();
        addObject(new TpKey(), 787,204);
        addObject(block,1085,686);
        block.setLocation(1129,587);
        Block block2 = new Block();
        addObject(block2,940,778);
        removeObject(block);
        block2.setLocation(901,717);
        removeObject(block2);
        Block block26 = new Block();
        addObject(block2,1120,799);
        Block block3 = new Block();
        addObject(block3,968,799);
        Block block4 = new Block();
        addObject(block4,807,799);
        block4.setLocation(813,783);
        block3.setLocation(898,799);
        Block block5 = new Block();
        addObject(block5,898,799);
        Block block6 = new Block();
        addObject(block6,656,799);
        Block block7 = new Block();
        addObject(block7,507,799);
        Block block8 = new Block();
        addObject(block8,354,799);
        Block block9 = new Block();
        addObject(block9,200,798);
        Block block10 = new Block();
        addObject(block10,73,799);
        block4.setLocation(764,745);
        block4.setLocation(774,578);
        removeObject(block4);
        Block block49 = new Block();
        addObject(block49,1019,799);
        Block block11 = new Block();
        addObject(block11,784,798);
        Player player = new Player();
        addObject(player,480,505);
        player.setLocation(179,412);
        removeObject(player);
        Platform platform = new Platform();
        addObject(platform,0,97);
        Player player2 = new Player();
        addObject(player2,11,25);
        Platform platform2 = new Platform();
        addObject(platform2,112,0);
        Platform platform3 = new Platform();
        addObject(platform3,112,58);
        Platform platform4 = new Platform();
        addObject(platform4,112,116);
        Platform platform5 = new Platform();
        addObject(platform5,112,174);
        Platform platform6 = new Platform();
        addObject(platform6,112,232);
        Platform platform7 = new Platform();
        addObject(platform7,112,290);
        Platform platform8 = new Platform();
        addObject(platform8,112,348);
        Platform platform9 = new Platform();
        addObject(platform9,112,406);
        Platform platform10 = new Platform();
        addObject(platform10,112,464);
        Platform platform11 = new Platform();
        addObject(platform11,112,522);
        Platform platform12 = new Platform();
        addObject(platform12,112,580);
        Platform platform13 = new Platform();
        addObject(platform13,112,637);
        Platform platform14 = new Platform();
        addObject(platform14,65,206);
        Platform platform15 = new Platform();
        addObject(platform15,65,264);
        Platform platform16 = new Platform();
        addObject(platform16,65,322);
        Platform platform17 = new Platform();
        addObject(platform17,7,321);
        removeObject(platform16);
        removeObject(platform15);

        Platform platform18 = new Platform();
        addObject(platform18,65,436);
        removeObject(platform15);
        removeObject(platform16);

        Platform platform19 = new Platform();
        addObject(platform19,7,552);
        platform16.setLocation(75,545);
        removeObject(platform15);
        removeObject(platform16);
        removeObject(platform18);
        removeObject(platform19);

        Platform platform20 = new Platform();
        addObject(platform20,82,580);
        Platform platform21 = new Platform();
        addObject(platform21,1,575);
        platform21.setLocation(13,562);
        removeObject(platform21);
        Platform platform212 = new Platform();
        addObject(platform212,0,579);
        removeObject(platform20);
        platform12.setLocation(92,580);
        Platform platform202 = new Platform();
        addObject(platform202,92,580);
        platform12.setLocation(112,579);
        Platform platform24 = new Platform();
        addObject(platform24,112,579);
        Platform platform25 = new Platform();
        addObject(platform25,227,690);
        Platform platform26 = new Platform();
        addObject(platform26,285,633);
        Platform platform27 = new Platform();
        addObject(platform27,169,581);
        Platform platform28 = new Platform();
        addObject(platform28,285,575);
        Platform platform29 = new Platform();
        addObject(platform29,285,519);
        Platform platform30 = new Platform();
        addObject(platform30,285,462);
        Platform platform31 = new Platform();
        addObject(platform31,227,462);
        Platform platform32 = new Platform();
        addObject(platform32,285,690);
        Platform platform33 = new Platform();
        addObject(platform33,285,404);
        Platform platform34 = new Platform();
       addObject(platform34,285,346);
        Platform platform35 = new Platform();
        addObject(platform35,285,288);
        Platform platform36 = new Platform();
        addObject(platform36,169,290);
        platform36.setLocation(180,351);
        platform36.setLocation(181,331);
        platform36.setLocation(176,323);
        platform36.setLocation(173,323);
        platform36.setLocation(188,336);
        platform36.setLocation(160,343);
        Platform platform37 = new Platform();
        addObject(platform37,285,230);
        Platform platform38 = new Platform();
        addObject(platform38,285,172);
        Platform platform39 = new Platform();
        addObject(platform39,285,114);
        Platform platform40 = new Platform();
        addObject(platform40,227,228);
        Platform platform41 = new Platform();
        addObject(platform41,169,106);
        Platform platform42 = new Platform();
        addObject(platform42,1067,427);
        platform42.setLocation(1077,418);
        removeObject(platform42);
        PowerPlat powerPlat = new PowerPlat();
        addObject(powerPlat,390,457);
        Platform platform421 = new Platform();
        addObject(platform421,342,577);
        Platform platform43 = new Platform();
        addObject(platform43,400,577);
        Platform platform44 = new Platform();
        addObject(platform44,458,577);
        Platform platform45 = new Platform();
        addObject(platform45,516,577);

        //
        Platform platform47 = new Platform();
        addObject(platform47,1199,582);
        Platform platform48 = new Platform();
        addObject(platform48,1141,581);
        Platform platform49 = new Platform();
        addObject(platform49,1084,581);
        Platform platform50 = new Platform();
        addObject(platform50,1029,581);
        Platform platform51 = new Platform();
        addObject(platform51,972,581);
        Platform platform52 = new Platform();
        addObject(platform52,972,524);
        Platform platform53 = new Platform();
        addObject(platform53,972,470);
        Platform platform54 = new Platform();
        addObject(platform54,1132,459);
        Platform platform55 = new Platform();
        addObject(platform55,1078,404);

        Platform platform56 = new Platform();
        addObject(platform56,1078,350);
        Platform platform57 = new Platform();
        addObject(platform57,1021,296);
        Platform platform58 = new Platform();
        addObject(platform58,1020,241);
        Platform platform59 = new Platform();
        addObject(platform59,1021,183);
        Platform platform60 = new Platform();
        addObject(platform60,1173,174);
        Platform platform61 = new Platform();
        addObject(platform61,1022,127);
        Platform platform62 = new Platform();
        addObject(platform62,1026,73);
        Platform platform63 = new Platform();
        addObject(platform63,1085,75);
        platform63.setLocation(1077,75);
        Platform platform64 = new Platform();
        addObject(platform64,1077,295);
        platform62.setLocation(1024,73);
        platform60.setLocation(1175,190);
    }

}
