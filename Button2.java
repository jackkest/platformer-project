import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * A transparent actor that if clicked switches the world to the about page.
 * 
 * @author Jack Kester
 * @version 1.0.0a
 */
public class Button2 extends Actor
{
    /**
     * Act - do whatever the Button2 wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        if(Greenfoot.mouseClicked(this))
        {
            Greenfoot.playSound("select.wav");
            Greenfoot.setWorld(new AboutPage());
        }
        
        MouseInfo m = Greenfoot.getMouseInfo();
        
        if(m != null)
        {
            if((m.getX() >= 516 && m.getX() <= 680) && (m.getY() >= 526 && m.getY() <= 576))
            {
                setImage("circle.png");
            }
        
            if(!((m.getX() >= 516 && m.getX() <= 680) && (m.getY() >= 526 && m.getY() <= 576)))
            {
                setImage("transparent.png");
            }
        }
    }    
}
