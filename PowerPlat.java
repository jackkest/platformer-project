import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * A special type of platform that spawns a powerup object.
 * 
 * @author Jack Kester
 * @version 1.0.5a
 */
public class PowerPlat extends Platform
{
    int i = 0;
    public void act() 
    {
        if(i == 0)
        {
            checkactivated();
        }
    }    
    
    public boolean checkactivated()
    {
        GreenfootImage powerplimage = getImage();
        if(getOneObjectAtOffset(0, ((powerplimage.getHeight() / 2) + 1), Player.class) != null)
        {
            i = 1;
            getWorld().addObject(new Powerup(), (getX() + powerplimage.getWidth()), getY());
            Greenfoot.playSound("powerplat.wav");
            return true;
        }
        /*
        if(getOneIntersectingObject(Player.class) != null)
        {
            i = 1;
            return true;
        }
        else
        {
            return false;
        }
        */
        return false;
    }
}
