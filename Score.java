import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * A class that deals with highscores.
 * 
 * @author Jack Kester 
 * @version 1.0.2a
 */
public class Score extends Actor
{
    public void act() 
    {
    }
    
    public void generateImage(int pos)
    {
        setImage(new GreenfootImage("Highscore: " + getWorld().getObjects(Counter.class).get(0).myInfo.getScore(), 24, Color.BLUE, Color.LIGHT_GRAY));
    }
}
