import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * A type of character that is controlled by the keyboard.
 * 
 * @author Jack Kester
 * @version 1.1.4a
 */
public class Player extends Character
{
    int i = 1;
    int i2 = 0;
    int i3 = 0;
    int cycle = 0;
    boolean powerup = false;
    int level = 1;  
    GreenfootSound scream = new GreenfootSound("scream.wav");
    GreenfootSound tp = new GreenfootSound("tpkey.wav");
    
    public void act() 
    {
        getKey();
        fall();
        checkCollision();
        checkKeys();
        unStuck();
        checkCoin();
        // Example of powerup 
        if(powerup)
        {
            move(10);
            if(cycle == 1)
            {
                launch();
                Greenfoot.playSound("powerup.wav");
            }
            if(cycle >= 35)
            {
                powerup = false;
                cycle = 0;
            }
            cycle++;
        }
    }   
    
    private void checkCollision()
    {
        if(getOneObjectAtOffset(0, -((Character.getHeight() / 2) + 1), Platform.class) != null)
        {
            vSpeed = 4;
            fall();
        }
        /*
        if(getOneObjectAtOffset(0, -((Character.getHeight() / 2) + 1), PowerPlat.class) != null)
        {
            PowerPlat power = (PowerPlat)getOneObjectAtOffset(0, -((Character.getHeight() / 2) + 4), PowerPlat.class);
            GreenfootImage powerimg = power.getImage();
            if(power.checkactivated() == false)
            {
                getWorld().addObject(new Powerup(), (power.getX()), (power.getY() - powerimg.getHeight()));
            }
            vSpeed = 4;
            fall();
        }
        */
        if(getOneObjectAtOffset((8), 0, Platform.class) != null)
        {
            canMoveRight = false;
        }
        else if(getOneObjectAtOffset((8), 0, Platform.class) == null)
        {
            canMoveRight = true;
        }
        if(getOneObjectAtOffset((-8), 0, Platform.class) != null)
        {
            canMoveLeft = false;
        }
        else if(getOneObjectAtOffset((-8), 0, Platform.class) == null)
        {
            canMoveLeft = true;
        }
        // Below only works in Player class, not in powerup class.
        if(isTouching(Powerup.class))
        {
            Powerup p = (Powerup)getOneIntersectingObject(Powerup.class);
            getWorld().removeObject(p);
            powerup = true;
        }
        
        if(getY() >= 900)
        {
            if(i2 == 0)
            {
                scream.play();
                i2 = 1;
            }
            if(!scream.isPlaying())
            {
                Greenfoot.setWorld(new Cover());
            }
        }
    }
    
    public void checkKeys()
    {
        if(Greenfoot.isKeyDown("up"))
        {  
            if(i == 1)
            {
                jump(); 
            }
            i = 0;
        }
        if(!Greenfoot.isKeyDown("up"))
        {
            i = 1;
        }
        if(Greenfoot.isKeyDown("down"))
        {
            // Add a method that makes player crouch
        }
        if(Greenfoot.isKeyDown("left") && canMoveLeft)
        {
            move(-5);
        }
        if(Greenfoot.isKeyDown("right") && canMoveRight)
        {
            move(5);
        }
    }

    public void jump()
    {
        if(isTouchingGround() || isTouchingPlat())
        {
            Greenfoot.playSound("jump.wav");
            vSpeed = -20;
            setLocation(getX(), getY() - 15);
            fall();
        }
    }
    
    public void launch()
    {
        if(isTouchingGround() || isTouchingPlat())
        {
            vSpeed = -20;
            setLocation(getX(), getY() - 15);
            fall();
        }
    }
    
    private void checkLevel()
    {
        if(getLevel() == 2)
        {
            Greenfoot.setWorld(new Level_2(getWorld().getObjects(Counter.class).get(0)));
        }
        
        if(getLevel() == 3)
        {
            Greenfoot.setWorld(new Level_3(getWorld().getObjects(Counter.class).get(0)));
        }
        
        if(getLevel() == 4)
        {
            Greenfoot.setWorld(new Level_4(getWorld().getObjects(Counter.class).get(0)));
        }
        
        if(getLevel() == 5)
        {
            Greenfoot.setWorld(new Level_5(getWorld().getObjects(Counter.class).get(0)));
        }
        
        if(getLevel() == 6)
        {
            Greenfoot.setWorld(new Level_6(getWorld().getObjects(Counter.class).get(0)));
        }
        
        if(getLevel() == 7)
        {
            Greenfoot.setWorld(new Level_7(getWorld().getObjects(Counter.class).get(0)));
        }
        
        if(getLevel() == 8)
        {
            Greenfoot.setWorld(new WinScreen());
        }
    }
    
    public void getKey()
    {
        if(isTouching(TpKey.class))
        {
            removeTouching(TpKey.class);
            tp.play();
            
            if(getLevel() == 1)
            {
                Greenfoot.setWorld(new Level_2(getWorld().getObjects(Counter.class).get(0)));
            }
            if(getLevel() == 2)
            {
                Greenfoot.setWorld(new Level_3(getWorld().getObjects(Counter.class).get(0)));
            }
            if(getLevel() == 3)
            {
                Greenfoot.setWorld(new Level_4(getWorld().getObjects(Counter.class).get(0)));
            }
            if(getLevel() == 4)
            {
                Greenfoot.setWorld(new Level_5(getWorld().getObjects(Counter.class).get(0)));
            }
            if(getLevel() == 5)
            {
                Greenfoot.setWorld(new Level_6(getWorld().getObjects(Counter.class).get(0)));
            }
            
            if(getLevel() == 6)
            {
                Greenfoot.setWorld(new Level_7(getWorld().getObjects(Counter.class).get(0)));
            }
            
            if(getLevel() == 7)
            {
                Greenfoot.setWorld(new Level_8(getWorld().getObjects(Counter.class).get(0)));
            }
            
            if(getLevel() == 8)
            {
                Greenfoot.setWorld(new WinScreen());
            }
        }
    }
     
    public int getLevel()
    {
        try
        {
            World world = (Level_1) getWorld();
            return 1;
        }
        catch (ClassCastException x) {}
        
        try
        {
            World world = (Level_2) getWorld();
            return 2;
        }
        catch (ClassCastException x) {}
        
        try
        {
            World world = (Level_3) getWorld();
            return 3;
        }
        catch (ClassCastException x) {}
        
        try
        {
            World world = (Level_4) getWorld();
            return 4;
        }
        catch (ClassCastException x) {}
        
         try
        {
            World world = (Level_5) getWorld();
            return 5;
        }
        catch (ClassCastException x) {}
        
        try
        {
            World world = (Level_6) getWorld();
            return 6;
        }
        catch (ClassCastException x) {}
        
        try
        {
            World world = (Level_7) getWorld();
            return 7;
        }
        catch (ClassCastException x) {}
        
        try
        {
            World world = (Level_8) getWorld();
            return 8;
        }
        catch (ClassCastException x) {}
        
        return 0;
    }
    
    public void checkCoin()
    {
        if(isTouching(Coin.class))
        {
            removeTouching(Coin.class);
            Greenfoot.playSound("coin.wav");
            getWorld().getObjects(Counter.class).get(0).increaseScore();
        }   
    }
    
    public int level()
    {
        return level;
    }
}
