
import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * A type of character that acts as a powerup if touched by the player.
 * 
 * @author Jack Kester 
 * @version 1.0.4a
 */
public class Powerup extends Character
{
    /**
     * Act - do whatever the Powerup wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    
    int i2 = 0;
    public void act() 
    {
        fall();
        checkCollision();
        unStuck();
    }    
    
    private void checkCollision()
    {
        if(getOneObjectAtOffset(0, -((Character.getHeight() / 2) + 1), Platform.class) != null)
        {
            vSpeed = 4;
            fall();
        }
        if(getOneObjectAtOffset((8), -4, Platform.class) != null)
        {
            canMoveRight = false;
        }
        else if(getOneObjectAtOffset((8), -4, Platform.class) == null)
        {
            canMoveRight = true;
        }
        if(getOneObjectAtOffset((-8), -4, Platform.class) != null)
        {
            canMoveLeft = false;
        }
        else if(getOneObjectAtOffset((-8), -4, Platform.class) == null)
        {
            canMoveLeft = true;
        }
        /*
        if(isTouching(Player.class))
        {
            Player player = (Player)getOneIntersectingObject(Player.class);
            getWorld().removeObject(this);
            //add powerup command here
        }
        */
    }
    
    public int getRandomNumber(int start,int end)
    {
       int normal = Greenfoot.getRandomNumber(end - start + 1);
       return normal + start;
    }
}
