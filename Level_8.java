import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Level_8 here.
 * 
 * @author Hank West
 * @version 1.0.1b
 */
public class Level_8 extends World
{

    /**
     * Constructor for objects of class Level_8.
     * 
     */
    public Level_8()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1200, 800, 1, false); 
        prepare();
    }
    
    public Level_8(Counter c)
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1200, 800, 1, false); 
        prepare();
        addObject(c, 1100, 12);
        
    }
    
    public void rainingtoliets()
    {
        int i = Greenfoot.getRandomNumber(1000);
        int j = Greenfoot.getRandomNumber(1000);
        int k = Greenfoot.getRandomNumber(1000);
        int l = Greenfoot.getRandomNumber(1000);
        int m = Greenfoot.getRandomNumber(1000);
        int n = Greenfoot.getRandomNumber(1000);
        int o = Greenfoot.getRandomNumber(1000);
        int p = Greenfoot.getRandomNumber(1000);
        if ( i < 15)
        {
            addObject(new Powerup(), 155, 0);
        }
        if ( j < 90)
        {
            addObject(new Powerup(), 280, 0);
        }
        if ( k < 40)
        {
            addObject(new Powerup(), 390, 0);
        }
        if ( l < 35)
        {
            addObject(new Powerup(), 505, 0);
        }
        if ( m < 30)
        {
            addObject(new Powerup(), 635, 0);
        }
        if ( n < 25)
        {
            addObject(new Powerup(), 740, 0);
        }
        if ( o < 20)
        {
            addObject(new Powerup(), 850, 0);
        }
        if ( p < 15)
        {
            addObject(new Powerup(), 975, 0);
        }
    }
    
    public void act()
    {
        rainingtoliets();
    }

    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare()
    {
       Platform platform = new Platform();
        addObject(platform,91,405);
        Platform platform2 = new Platform();
        addObject(platform2,207,405);
        Platform platform3 = new Platform();
        addObject(platform3,328,405);
        Platform platform4 = new Platform();
        addObject(platform4,445,407);
        Platform platform5 = new Platform();
        addObject(platform5,564,407);
        Platform platform6 = new Platform();
        addObject(platform6,680,406);
        Platform platform7 = new Platform();
        addObject(platform7,799,407);
        Platform platform8 = new Platform();
        addObject(platform8,916,406);
        Platform platform9 = new Platform();
        addObject(platform9,1035,406);
        TpKey tpkey = new TpKey();
        addObject(tpkey,1102,464);
        Player player = new Player();
        addObject(player,89,106);
        Coin coin = new Coin();
        addObject(coin,154,282);
        Coin coin2 = new Coin();
        addObject(coin2,278,282);
        Coin coin3 = new Coin();
        addObject(coin3,390,287);
        Coin coin4 = new Coin();
        addObject(coin4,504,284);
        Coin coin5 = new Coin();
        addObject(coin5,634,284);
        Coin coin6 = new Coin();
        addObject(coin6,742,290);
        Coin coin7 = new Coin();
        addObject(coin7,857,290);
        Coin coin8 = new Coin();
        addObject(coin8,977,287);
        Coin coin9 = new Coin();
        addObject(coin9,1149,399);
        Coin coin10 = new Coin();
        addObject(coin10,1154,343);
        Coin coin11 = new Coin();
        addObject(coin11,1106,292);
        tpkey.setLocation(96,700);
        Platform platform10 = new Platform();
        addObject(platform10,1033,700);
        Platform platform11 = new Platform();
        addObject(platform11,919,701);
        Platform platform12 = new Platform();
        addObject(platform12,800,701);
        Platform platform13 = new Platform();
        addObject(platform13,681,702);
        Platform platform14 = new Platform();
        addObject(platform14,564,700);
        Platform platform15 = new Platform();
        addObject(platform15,444,703);
        Platform platform16 = new Platform();
        addObject(platform16,326,702);
        Platform platform17 = new Platform();
        addObject(platform17,210,701);
        Platform platform18 = new Platform();
        addObject(platform18,30,406);
        Platform platform19 = new Platform();
        addObject(platform19,32,465);
        Platform platform20 = new Platform();
        addObject(platform20,32,526);
        Platform platform21 = new Platform();
        addObject(platform21,32,585);
        Platform platform22 = new Platform();
        addObject(platform22,32,642);
        Platform platform23 = new Platform();
        addObject(platform23,31,704);
        Platform platform24 = new Platform();
        addObject(platform24,32,761);
        platform19.setLocation(31,463);
        platform18.setLocation(33,405);
        platform19.setLocation(35,462);
        platform19.setLocation(34,461);
        platform20.setLocation(35,517);
        platform20.setLocation(34,517);
        platform21.setLocation(35,574);
        platform22.setLocation(35,632);
        platform23.setLocation(34,690);
        platform24.setLocation(34,748);
        platform22.setLocation(33,632);
        platform21.setLocation(34,575);
        Platform platform25 = new Platform();
        addObject(platform25,151,465);
        platform25.setLocation(150,463);
        platform25.setLocation(147,457);
        platform25.setLocation(149,461);
        platform25.setLocation(918,643);
        Platform platform26 = new Platform();
        addObject(platform26,679,645);
        platform26.setLocation(681,646);
        Platform platform27 = new Platform();
        addObject(platform27,445,648);
        Platform platform28 = new Platform();
        addObject(platform28,210,644);
        platform27.setLocation(444,648);
        Platform platform29 = new Platform();
        addObject(platform29,151,408);
        platform29.setLocation(149,404);
    }

}
