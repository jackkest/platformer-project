import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Level_5 here.
 * 
 * @author Hank West
 * @version 1.0.1b
 */
public class Level_5 extends World
{

    /**
     * Constructor for objects of class Level_5.
     * 
     */
    public Level_5(Counter c)
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1200, 800, 1, false); 
        prepare();
        addObject(c, 1100, 12);
    }
    
    public Level_5()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1200, 800, 1, false);
        prepare();
    }
    
    private void prepare()
    {
        Platform platform = new Platform();
        addObject(platform,92,289);
        platform.setLocation(89,287);
        Platform platform2 = new Platform();
        addObject(platform2,444,288);
        platform2.setLocation(444,285);
        Platform platform3 = new Platform();
        addObject(platform3,799,287);
        platform3.setLocation(798,287);
        Platform platform4 = new Platform(); 
        addObject(platform4,1153,230);
        Platform platform5 = new Platform();
        addObject(platform5,1146,167);
        platform5.setLocation(1151,169);
        platform4.setLocation(1151,226);
        Powerup powerup = new Powerup(); 
        addObject(powerup,803,60);
        Powerup powerup2 = new Powerup();
        addObject(powerup2,450,57);
        Powerup powerup3 = new Powerup();
        addObject(powerup3,96,118);
        powerup2.setLocation(443,-700);
        powerup.setLocation(803,-1400);
        Player player = new Player();
        addObject(player,95,49);
        platform5.setLocation(1152,698);
        removeObject(platform4);
        Platform platform6 = new Platform();
        addObject(platform6,858,703);
        Platform platform7 = new Platform(); 
        addObject(platform7,563,702);
        Platform platform8 = new Platform();
        addObject(platform8,267,703);
        TpKey tpkey = new TpKey();
        addObject(tpkey,562,407);
        Platform platform9 = new Platform();
        addObject(platform9,30,700);
        tpkey.setLocation(212,454);
        Platform platform10 = new Platform();
        addObject(platform10,209,525);
        Powerup powerup4 = new Powerup();
        addObject(powerup4,30,500);
        platform10.setLocation(209,504);
        tpkey.setLocation(215,397);
        platform10.setLocation(208,467);
        platform10.setLocation(443,406);
        tpkey.setLocation(446,345);
        platform6.setLocation(918,703);
        platform7.setLocation(680,703);
        platform8.setLocation(445,698);
        platform9.setLocation(210,699);
        powerup4.setLocation(148,462);
        Platform platform11 = new Platform();
        addObject(platform11,151,526);
        platform11.setLocation(147,524);
        platform11.setLocation(90,519);
        powerup4.setLocation(87,457);
        platform11.setLocation(90,521);
        Platform platform12 = new Platform();
        addObject(platform12,150,525);
        platform12.setLocation(148,523);
        platform11.setLocation(91,522);
    }
}
