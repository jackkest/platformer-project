import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Level_7 here.
 * 
 * @author Caneron Honea and Sean O'Connor
 * @version 1.0.1b
 */
public class Level_7 extends World
{

    /**
     * Constructor for objects of class Level_7.
     * 
     */
    public Level_7(Counter c)
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1200, 800, 1, false); 
        prepare();
        addObject(c, 1100, 12);
    }
    
    private void prepare()
    {
        Platform platform = new Platform();
        addObject(platform,225,267);
        Platform platform2 = new Platform();
        addObject(platform2,383,253);
        Platform platform3 = new Platform();
        addObject(platform3,623,242);
        Platform platform4 = new Platform();
        addObject(platform4,836,254);
        Platform platform5 = new Platform();
        addObject(platform5,909,345);
        Platform platform6 = new Platform();
        addObject(platform6,806,500);
        Platform platform7 = new Platform();
        addObject(platform7,536,506);
        Platform platform8 = new Platform();
        addObject(platform8,639,402);
        Platform platform9 = new Platform();
        addObject(platform9,485,377);
        Platform platform10 = new Platform();
        addObject(platform10,294,406);
        Platform platform11 = new Platform();
        addObject(platform11,223,481);
        Platform platform12 = new Platform();
        addObject(platform12,268,556);
        Platform platform13 = new Platform();
        addObject(platform13,411,592);
        Platform platform14 = new Platform();
        addObject(platform14,578,645);
        Platform platform15 = new Platform();
        addObject(platform15,743,650);
        Platform platform16 = new Platform();
        addObject(platform16,892,621);
        Platform platform17 = new Platform();
        addObject(platform17,1012,505);
        Platform platform18 = new Platform();
        addObject(platform18,1041,363);
        Platform platform19 = new Platform();
        addObject(platform19,1043,241);
        Platform platform20 = new Platform();
        addObject(platform20,856,130);
        Platform platform21 = new Platform();
        addObject(platform21,611,83);
        Platform platform22 = new Platform();
        addObject(platform22,401,69);
        Platform platform23 = new Platform();
        addObject(platform23,243,76);
        Platform platform24 = new Platform();
        addObject(platform24,101,169);
        Platform platform25 = new Platform();
        addObject(platform25,62,301);
        Platform platform26 = new Platform();
        addObject(platform26,77,415);
        Platform platform27 = new Platform();
        addObject(platform27,97,530);
        Platform platform28 = new Platform();
        addObject(platform28,172,620);
        Platform platform29 = new Platform();
        addObject(platform29,353,709);
        Platform platform30 = new Platform();
        addObject(platform30,506,737);
        Platform platform31 = new Platform();
        addObject(platform31,679,750);
        Platform platform32 = new Platform();
        addObject(platform32,780,747);
        Platform platform33 = new Platform();
        addObject(platform33,959,740);
        Platform platform34 = new Platform();
        addObject(platform34,1080,690);
        platform27.setLocation(31,758);
        platform28.setLocation(209,758);
        platform11.setLocation(209,700);
        platform12.setLocation(209,642);
        platform10.setLocation(209,584);
        platform26.setLocation(209,526);
        platform.setLocation(209,468);
        platform25.setLocation(208,410);
        platform25.setLocation(209,411);
        platform24.setLocation(209,351);
        platform24.setLocation(209,354);
        platform23.setLocation(209,294);
        platform23.setLocation(209,296);
        platform2.setLocation(209,238);
        platform22.setLocation(209,180);
        platform21.setLocation(209,123);
        platform20.setLocation(856,130);
        platform20.setLocation(856,130);
        platform20.setLocation(856,130);
        platform20.setLocation(856,130);
        platform20.setLocation(856,130);
        platform20.setLocation(856,130);
        platform20.setLocation(856,130);
        platform20.setLocation(856,130);
        platform20.setLocation(856,130);
        platform20.setLocation(856,130);
        platform20.setLocation(856,130);
        platform20.setLocation(856,130);
        platform20.setLocation(856,130);
        platform20.setLocation(856,130);
        platform20.setLocation(856,130);
        platform20.setLocation(856,130);
        platform20.setLocation(856,130);
        platform20.setLocation(856,130);
        platform20.setLocation(856,130);
        platform20.setLocation(856,130);
        platform20.setLocation(856,130);
        platform20.setLocation(856,130);
        platform20.setLocation(856,130);
        platform20.setLocation(856,130);
        platform20.setLocation(856,130);
        platform20.setLocation(856,130);
        platform20.setLocation(856,130);
        platform20.setLocation(498,57);
        platform20.setLocation(209,65);
        platform3.setLocation(209,8);
        platform3.setLocation(208,8);
        platform3.setLocation(209,7);
        platform29.setLocation(277,758);
        platform30.setLocation(528,759);
        platform32.setLocation(839,593);
        platform31.setLocation(785,758);
        platform31.setLocation(781,759);
        platform33.setLocation(1061,759);
        platform34.setLocation(1153,583);
        platform32.setLocation(892,396);
        platform6.setLocation(664,202);
        platform8.setLocation(559,240);
        platform9.setLocation(486,185);
        platform7.setLocation(387,263);
        platform13.setLocation(404,216);
        platform14.setLocation(432,187);
        platform15.setLocation(527,229);
        platform16.setLocation(691,267);
        platform32.setLocation(821,288);
        platform5.setLocation(895,232);
        platform17.setLocation(1034,525);
        platform17.setLocation(1035,522);
        platform18.setLocation(774,522);
        platform32.setLocation(515,523);
        platform16.setLocation(326,463);
        platform7.setLocation(240,293);
        platform14.setLocation(426,81);
        platform9.setLocation(569,52);
        platform13.setLocation(348,68);
        platform15.setLocation(656,78);
        platform8.setLocation(792,97);
        platform6.setLocation(832,90);
        platform4.setLocation(923,134);
        platform5.setLocation(1035,113);
        platform19.setLocation(386,227);
        platform14.setLocation(649,227);
        platform8.setLocation(898,228);
        platform4.setLocation(1158,228);
        platform5.setLocation(1081,64);
        platform6.setLocation(1002,63);
        platform15.setLocation(921,62);
        platform9.setLocation(838,61);
        platform13.setLocation(691,63);
        platform13.setLocation(690,61);
        TpKey tpkey = new TpKey();
        addObject(tpkey,701,19);
        tpkey.setLocation(701,18);
        Player player = new Player();
        addObject(player,161,57);
        player.setLocation(155,49);
        Powerup powerup = new Powerup();
        addObject(powerup,46,-1000);
        platform33.setLocation(1051,758);
        platform33.setLocation(1041,760);
        platform14.setLocation(639,229);
        platform17.setLocation(1018,523);
        Coin coin = new Coin();
        addObject(coin,518,468);
        Coin coin2 = new Coin();
        addObject(coin2,779,463);
        Coin coin3 = new Coin();
        addObject(coin3,1028,461);
        Coin coin4 = new Coin();
        addObject(coin4,833,8);
        Coin coin5 = new Coin();
        addObject(coin5,910,6);
        Coin coin6 = new Coin();
        addObject(coin6,1008,9);
        Coin coin7 = new Coin();
        addObject(coin7,1078,11);
        platform17.setLocation(995,522);
        coin3.setLocation(1000,464);
        platform29.setLocation(267,759);
        platform29.setLocation(266,758);
    }
}
